package HttpMessage

import "strings"

type Header struct {
	Name		string
	Value 		string
}

type HeaderCollection struct {
	collection 		map[string][]*Header
}

func (col *HeaderCollection) init() {
	if col.collection == nil {
		col.collection = make(map[string][]*Header)
	}
}

func (col *HeaderCollection) Add(h *Header) {
	col.init()

	col.collection[h.Name] = append(col.collection[h.Name], h)
}

func (col *HeaderCollection) GetMap() map[string][]*Header {
	col.init()
	return col.collection
}

func (col *HeaderCollection) GetConsolidatedMap() map[string]string {
	col.init()

	ret := make(map[string]string)

	//var values []string
	for name, headers := range col.collection {
		values := []string{}
		for _, header := range headers {
			values = append(values, header.Value)
		}

		ret[name] = strings.Join(values, ", ")
	}

	return ret
}
