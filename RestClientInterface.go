package HttpMessage

type RestClientInterface interface {
	SendRequest(req *Request) Response
}

